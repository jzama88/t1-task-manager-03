package com.t1.alieva.tm;

import com.t1.alieva.tm.constant.TerminalConst;

import static com.t1.alieva.tm.constant.TerminalConst.*;

public class Application {
    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showErrorArgument();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void showErrorArgument() {
        System.err.println("Error! This argument is not supported...  ");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:Alieva Djamilya");
        System.out.println("E-mail:jzama88@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application version.\n", VERSION);
        System.out.printf("%s - Show developer info.\n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application commands.\n", TerminalConst.HELP);
    }
}
