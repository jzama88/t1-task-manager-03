# TASK-MANAGER

## DEVELOPER

NAME: Alieva Djamilya

E-MAIL: jzama88@gmail.com

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Microsoft Windows 10 Corporation

## HARDWARE

**CPU**: i5

**RAM**: 16Gb

**SSD**: 256Gb

## APPLICATION RUN

```bash
java jar - ./task-manager.jar
```
